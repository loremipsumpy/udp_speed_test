# README #

Speed test hecho con python por medio de sockets y puertos UDP **para ayuda ejecutar python3 server.py -h** o analogamente python3 client.py -h  
### ¿Cómo funciona?###

* Se hace deploy del servidor, se le asigna una ip y puerto a cual responder.
* El cliente se conecta a la ip y puerto que escucha el servidor, y le envía tantas tramas como se le especifique de tamaño también especificado.
* El servidor recibe las tramas y marca el tiempo de la primera trama, una vez que recibe todas hace la diferencia de los tiempos y calcula la velocidad de transferencia con respecto al tamaño total de las tramas.
### ¿Cómo pongo a correr el servidor? ###

* Ubicar la consola en la raíz del proyecto y ejecutar con python el archivo server.py ( Los parámetros que recibe están especificados python3 server.py -h )
### ¿Cómo pongo a correr el cliente? ###

* Ubicar la consola en la raíz del proyecto y ejecutar con python el archivo client.py ( Los parámetros que recibe están especificados python3 client.py -h)
###Ejemplo servidor###

* python3 server.py 127.0.0.1 3584 0
###Ejemplo del cliente###

* python3 client.py 127.0.0.1 3584 10 500
###Cosas por hacer###
* Encontrar por que rayos el servidor recibe una trama más de lo especificado.
* Cambiar la sentencia While del servidor para que no consuma tanto procesador.
* Enviar datos Json en vez de simples datagramas